﻿using DAL;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.DAL
{
    public class HomeDAL:BaseDAL
    {
        public stam_Client VerifyClientLogin(stam_Client login)
        {
            try
            {
                stam_Client isLogin = new stam_Client();
               // int loginId = 0;
               
                isLogin = (from res in _db.stam_Client
                           where res.UserName.Equals(login.UserName)
                           select res).FirstOrDefault();
                if (isLogin != null)
                {

                    if (login.Password == null)
                    {
                        var getLogin = _db.stam_Client.Where(x => x.UserName == isLogin.UserName).FirstOrDefault();
                        if (!getLogin.Password.Equals(isLogin.Password))
                        {
                            isLogin = null;
                        }
                    }
                    else
                    {
                        if (!login.Password.Equals(isLogin.Password))
                        {
                            isLogin = null;
                        }
                    }
                }
                
                return isLogin;
            }
            catch
            {
                throw;
            }
        }

        #region Company
        public int SaveCompany(stam_Companies comp)
        {
            try
            {
                //int Status = machine.Id;
                int Status = 0;
                //if (IsSubGroupExist(subGroup.Id, subGroup.GroupId, subGroup.Name))
                //{

                if (comp.CompanyId == 0)
                {
                    //user.role_id = 1;
                    ////  user.school_id = 1;
                    //user.status = true;
                    //user.created_at = DateTime.Now;
                    _db.stam_Companies.Add(comp);
                    _db.SaveChanges();
                    Status = comp.CompanyId;

                    stam_Companies getResult = (from res in _db.stam_Companies
                                      where res.CompanyId == Status
                                      select res).FirstOrDefault();
                    if (getResult != null)
                    {
                        getResult.CompanyId = Status;
                        _db.SaveChanges();
                    }
                }
                else
                {
                    stam_Companies getResult = (from res in _db.stam_Companies
                                      where res.CompanyId == comp.CompanyId
                                      select res).FirstOrDefault();
                    if (getResult != null)
                    {
                        getResult.Name = comp.Name;
                        getResult.CountryCode = comp.CountryCode;
                        getResult.TraderID = comp.TraderID;
                        getResult.Address = comp.Address;
                        getResult.PostalCode = comp.PostalCode;
                        getResult.City = comp.City;
                        getResult.CountryCode = comp.CountryCode;
                        getResult.Email = comp.Email;
                        getResult.Phone = comp.Phone;
                        getResult.PeachActive = comp.PeachActive;
                        _db.SaveChanges();
                        Status = getResult.CompanyId;
                    }
                }

                return Status;
            }
            catch(Exception ex)
            {
                throw;
            }

        }
        public List<stam_Companies> GetCompanyDetail(int RoleId)
        {
            try
            {
                List<stam_Companies> userdetails = _db.stam_Companies.OrderBy(x => x.Name).ToList();
                return userdetails;
            }
            catch
            {
                throw;
            }
        }

        public bool IsCompanyNameExist(string UserName)
        {
            try
            {
                stam_Companies isLogin = new stam_Companies();
                bool IsUserName = false;

                isLogin = (from res in _db.stam_Companies
                           where res.Name.Equals(UserName)
                           select res).FirstOrDefault();
                if (isLogin != null)
                {
                    IsUserName = true;
                }

                return IsUserName;
            }
            catch
            {
                throw;
            }
        }


        public bool DeleteCompany(int Id)
        {
            try
            {
                bool Status = false;
                stam_Companies DeleteCompany = new stam_Companies();
                DeleteCompany = _db.stam_Companies.Where(a => a.CompanyId == Id).FirstOrDefault();
                if (DeleteCompany != null)
                {
                    _db.stam_Companies.Remove(DeleteCompany);
                    // DeleteADTReg.IsDeleted = true;
                    // bool result = SaveAuditLog(AuditTr.GetAuditProperties<HajjERADTReg>(new HajjERADTReg { IsDeleted = false }, new HajjERADTReg { IsDeleted = true }, 3, DeleteADTReg.Id, Convert.ToInt32(DeleteADTReg.ModifiedBy)));
                    Status = true;
                }
                _db.SaveChanges();
                return Status;
            }
            catch
            {
                throw;
            }
        }

        public stam_Companies GetCompanyById(int Cid)
        {
            try
            {
                stam_Companies userdetails = _db.stam_Companies.Where(x => x.CompanyId == Cid).FirstOrDefault();
                return userdetails;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region User
        public int SaveUser(stam_Users obj)
        {
            try
            {
                //int Status = machine.Id;
                int Status = 0;
                //if (IsSubGroupExist(subGroup.Id, subGroup.GroupId, subGroup.Name))
                //{

                if (obj.Id == 0)
                {
                    //user.role_id = 1;
                    ////  user.school_id = 1;
                    //user.status = true;
                    //user.created_at = DateTime.Now;
                    _db.stam_Users.Add(obj);
                    _db.SaveChanges();
                    Status = obj.Id;

                    stam_Users getResult = (from res in _db.stam_Users
                                                where res.Id == Status
                                                select res).FirstOrDefault();
                    if (getResult != null)
                    {
                        getResult.Id = Status;
                        _db.SaveChanges();
                    }
                }
                else
                {
                    stam_Users getResult = (from res in _db.stam_Users
                                                where res.Id == obj.Id
                                                select res).FirstOrDefault();
                    if (getResult != null)
                    {
                        getResult.clientCode = obj.clientCode;
                        getResult.UserID = obj.UserID;
                        getResult.Username = obj.Username;
                        getResult.Password = obj.Password;
                        getResult.Email = obj.Email;
                        getResult.Remark = obj.Remark;
                        _db.SaveChanges();
                        Status = getResult.Id;
                    }
                }

                return Status;
            }
            catch(Exception ex)
            {
                throw;
            }

        }
        public List<stam_Users> GetUserDetail()
        {
            try
            {
                List<stam_Users> userdetails = _db.stam_Users.OrderBy(x => x.Id).ToList();
                return userdetails;
            }
            catch
            {
                throw;
            }
        }

        public bool IsUserNameExist(string UserName)
        {
            try
            {
                stam_Users isLogin = new stam_Users();
                bool IsUserName = false;

                isLogin = (from res in _db.stam_Users
                           where res.clientCode.Equals(UserName)
                           select res).FirstOrDefault();
                if (isLogin != null)
                {
                    IsUserName = true;
                }

                return IsUserName;
            }
            catch
            {
                throw;
            }
        }


        public bool DeleteUser(int Id)
        {
            try
            {
                bool Status = false;
                stam_Users DeleteCompany = new stam_Users();
                DeleteCompany = _db.stam_Users.Where(a => a.Id == Id).FirstOrDefault();
                if (DeleteCompany != null)
                {
                    _db.stam_Users.Remove(DeleteCompany);
                    // DeleteADTReg.IsDeleted = true;
                    // bool result = SaveAuditLog(AuditTr.GetAuditProperties<HajjERADTReg>(new HajjERADTReg { IsDeleted = false }, new HajjERADTReg { IsDeleted = true }, 3, DeleteADTReg.Id, Convert.ToInt32(DeleteADTReg.ModifiedBy)));
                    Status = true;
                }
                _db.SaveChanges();
                return Status;
            }
            catch
            {
                throw;
            }
        }

        public stam_Users GetUserById(int Cid)
        {
            try
            {
                stam_Users userdetails = _db.stam_Users.Where(x => x.Id == Cid).FirstOrDefault();
                return userdetails;
            }
            catch
            {
                throw;
            }
        }
        #endregion


        #region Customer
        public stam_Users VerifyCustomerLogin(stam_Users login)
        {
            try
            {
                stam_Users isLogin = new stam_Users();
                // int loginId = 0;

                isLogin = (from res in _db.stam_Users
                           where res.Username.Equals(login.Username)
                           select res).FirstOrDefault();
                if (isLogin != null)
                {

                    if (login.Password == null)
                    {
                        var getLogin = _db.stam_Users.Where(x => x.Username == isLogin.Username).FirstOrDefault();
                        if (!getLogin.Password.Equals(isLogin.Password))
                        {
                            isLogin = null;
                        }
                    }
                    else
                    {
                        if (!login.Password.Equals(isLogin.Password))
                        {
                            isLogin = null;
                        }
                    }
                }

                return isLogin;
            }
            catch
            {
                throw;
            }
        }
        public int SaveCustomer(party obj)
        {
            try
            {
                //int Status = machine.Id;
                int Status = 0;
                //if (IsSubGroupExist(subGroup.Id, subGroup.GroupId, subGroup.Name))
                //{

                if (obj.PartiesId == 0)
                {
                    //Customer.role_id = 1;
                    ////  Customer.school_id = 1;
                    //Customer.status = true;
                    //Customer.created_at = DateTime.Now;
                    _db.parties.Add(obj);
                    _db.SaveChanges();
                    Status = obj.PartiesId;

                    party getResult = (from res in _db.parties
                                                where res.PartiesId == Status
                                                select res).FirstOrDefault();
                    if (getResult != null)
                    {
                        getResult.PartiesId = Status;
                        _db.SaveChanges();
                    }
                }
                else
                {
                    party getResult = (from res in _db.parties
                                                where res.PartiesId == obj.PartiesId
                                                select res).FirstOrDefault();
                    if (getResult != null)
                    {
                        getResult.localReference = obj.localReference;
                        getResult.commercialReference = obj.commercialReference;
                        getResult.partyType = obj.partyType;
                        getResult.customsProcess = obj.customsProcess;
                        getResult.traderID = obj.traderID;
                        getResult.vatID = obj.vatID;
                        getResult.companyCode = obj.companyCode;
                        getResult.name = obj.name;
                        getResult.street = obj.street;
                        getResult.postalCode = obj.postalCode;
                        getResult.city = obj.city;
                        getResult.countryCode = obj.countryCode;
                        _db.SaveChanges();
                        Status = getResult.PartiesId;
                    }
                }

                return Status;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public List<party> GetCustomerDetail(int RoleId)
        {
            try
            {
                List<party> Customerdetails = _db.parties.OrderBy(x => x.PartiesId).ToList();
                return Customerdetails;
            }
            catch
            {
                throw;
            }
        }

        public bool IsCustomerNameExist(string CustomerName)
        {
            try
            {
                party isLogin = new party();
                bool IsCustomerName = false;

                isLogin = (from res in _db.parties
                           where res.localReference.Equals(CustomerName)
                           select res).FirstOrDefault();
                if (isLogin != null)
                {
                    IsCustomerName = true;
                }

                return IsCustomerName;
            }
            catch
            {
                throw;
            }
        }


        public bool DeleteCustomer(int Id)
        {
            try
            {
                bool Status = false;
                party DeleteCompany = new party();
                DeleteCompany = _db.parties.Where(a => a.PartiesId == Id).FirstOrDefault();
                if (DeleteCompany != null)
                {
                    _db.parties.Remove(DeleteCompany);
                    // DeleteADTReg.IsDeleted = true;
                    // bool result = SaveAuditLog(AuditTr.GetAuditProperties<HajjERADTReg>(new HajjERADTReg { IsDeleted = false }, new HajjERADTReg { IsDeleted = true }, 3, DeleteADTReg.Id, Convert.ToInt32(DeleteADTReg.ModifiedBy)));
                    Status = true;
                }
                _db.SaveChanges();
                return Status;
            }
            catch
            {
                throw;
            }
        }

        public party GetCustomerById(int Cid)
        {
            try
            {
                party Customerdetails = _db.parties.Where(x => x.PartiesId == Cid).FirstOrDefault();
                return Customerdetails;
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
}
