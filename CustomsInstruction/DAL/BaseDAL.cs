﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;
namespace DAL
{
    public class BaseDAL : IDisposable
    {
        internal TranslimaEntities _db;

        public BaseDAL()
        {
            try
            {
                _db = new TranslimaEntities();
            }
            catch
            {
            }
        }

        //~BaseDAL()
        //{
        //    this.CleanUp();
        //}

        public void Dispose()
        {
            this.CleanUp();
            GC.SuppressFinalize(this);
        }

        private void CleanUp()
        {
            //if (_db != null) _db.Dispose();
        }
        //public bool SaveAuditLog(List<Audit> AuditList)
        //{
        //    bool result = false;
        //    try
        //    {
        //        foreach (Audit audit in AuditList)
        //        {
        //            _db.Audits.Add(audit);
        //        }
        //        _db.SaveChanges();
        //        return result;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
    }
}
