﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class InstructionDAL:BaseDAL
    {
        public int SaveInstruction(commercial commercial, party partyObj, commercial_extra commercial_extraObj, item itemobj, items_packages itemPackeagesObj, items_extra items_extra)
        {
            try
            {
                //int Status = machine.Id;
                int Status = 1;
                //if (IsSubGroupExist(subGroup.Id, subGroup.GroupId, subGroup.Name))
                //{

                //if (commercial.CompanyId == 0)
                //{
                //user.role_id = 1;
                ////  user.school_id = 1;
                //user.status = true;
                //user.created_at = DateTime.Now;
                commercial.localReference = "AY3CGC00B31000350301";
                commercial.status = "New";

                 _db.commercials.Add(commercial);
                 _db.SaveChanges();

                _db.parties.Add(partyObj);
                _db.SaveChanges();
               
                _db.commercial_extra.Add(commercial_extraObj);
                _db.SaveChanges();

                _db.items.Add(itemobj);
                _db.SaveChanges();

                itemPackeagesObj.itemsID = itemobj.itemsID;
                _db.items_packages.Add(itemPackeagesObj);
                _db.SaveChanges();

                items_extra.itemsID = itemobj.itemsID;
                _db.items_extra.Add(items_extra);
                _db.SaveChanges();
                //   Status = comp.CompanyId;



                return Status;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
