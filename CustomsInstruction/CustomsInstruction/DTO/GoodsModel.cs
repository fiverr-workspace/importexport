﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomsInstruction.DTO
{
    public class GoodsModel
    {
     public string materialNumber { get; set; }
     public string packagesMarks { get; set; }
        public string quantity { get; set; }
        public string quantityUOM { get; set; }
        public string packagesNumber { get; set; }
        public string packagesType { get; set; }
        public string goodsDescription { get; set; }
        public string value { get; set; }
        public string tariffNumber { get; set; }
        public string originCountryCode { get; set; }
        public string grossWeight { get; set; }
        public string packagesGrossWeight { get; set; }
        public string packagesGrossWeightUnit { get; set; }
        public string netWeight { get; set; }
        public string packagesNetWeight { get; set; }
        public string packagesNetWeightUnit { get; set; }
        public string netPriceCurrency { get; set; }
        public string netPrice { get; set; }
        public string PEACH_PHYTO { get; set; }
        public string PEACH_COMGROUP { get; set; }
        public string PEACH_VARIATY { get; set; }
        public string PEACH_GROWER { get; set; }
    }
}