﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.EF;

namespace CustomsInstruction.Models
{
    public class InstructionViewModel
    {
        public item Item {get;set;}
        public items_extra items_extra { get; set; }
        public  items_packages itemPackeagesObj { get; set; }
        public commercial commercial { get; set; }

        public commercial_extra commercial_extraObj { get; set; }

        public party partyObj { get; set; }

        public IEnumerable<SelectListItem> ddlImp_Exp_Cons_List { get; set; }

        public IEnumerable<SelectListItem> PlanPassportList { get; set; }

        public IEnumerable<SelectListItem> PackageTypeList { get; set; }
    }


}