﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace e_klass.Models
{
    public class HomeViewModel
    {
        public stam_Client client { get; set;}
        
        public stam_Companies CompanyObj { get; set; }

        public stam_Users UserObj { get; set; }

        public party CustomerObj { get; set; }
        public string ErrorMessage { get; set; }

        public string ResponseMessage { get; set; }
        public string ResponseColour { get; set; }

        public List<stam_Companies> GetCompanyList { get; set; }

        public List<stam_Users> GetUserList { get; set; }

        public List<party> GetCustomerList { get; set; }

        public IEnumerable<SelectListItem> ddlPartiesList { get; set; }

        public IEnumerable<SelectListItem> ddlRoleList { get; set; }

    }
}