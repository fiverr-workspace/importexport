﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using CustomsInstruction.Models;
using CustomsInstruction.Utility;
using Model.EF;
using Services.BLL;

namespace CustomsInstruction.Controllers
{
    public class InstructionController : Controller
    {
        private InstructionViewModel _viewModel;
        MVCUtility utility = new MVCUtility();
        InstructionBLL instructionBLL = new InstructionBLL();
        // GET: Instruction

        public InstructionController()
        {
            _viewModel = new InstructionViewModel();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Instruction()
        {
            HomeBLL homeBLL = new HomeBLL();
            _viewModel.ddlImp_Exp_Cons_List = utility.GetUserList();
            List<SelectListItem> list = new List<SelectListItem> {
                        new SelectListItem {Text="-- Select --",Value="" },
                        new SelectListItem {Text="Yes",Value="Yes" },
                        new SelectListItem {Text="No",Value="No" }
                        };
            _viewModel.PlanPassportList = list;

            List<SelectListItem> pkglist = new List<SelectListItem> {
                        new SelectListItem {Text="-- Select --",Value="" },
                        new SelectListItem {Text="CT",Value="CT" },
                        new SelectListItem {Text="PX",Value="PX" },
                        new SelectListItem {Text="PP",Value="PP" },
                        new SelectListItem {Text="PK",Value="PK" }
                        };
            _viewModel.PackageTypeList = pkglist;
            return View(_viewModel);
        }
        public ActionResult SaveInstruction(commercial commercial, party partyObj, commercial_extra commercial_extraObj, item itemobj, items_packages itemPackeagesObj, items_extra items_extra)
        {
            //item itemobj = new item();
            //items_extra items_ExtraObj = new items_extra();
            //items_packages items_PackageObj = new items_packages();
            //commercial commercialObj = new commercial();
            //commercial_extra commercial_ExtraObj = new commercial_extra();
            //party partyObj = new party();


            int Status = instructionBLL.SaveInstruction(commercial, partyObj, commercial_extraObj, itemobj, itemPackeagesObj, items_extra);


           // var result = viewModel;
            return View(_viewModel);
        }

        #region Excel file work
        //[HttpPost]
        //public JsonResult SaveExcel()
        //{

        //    List<ExcelViewModel> excelData = new List<ExcelViewModel>();
        //    if (Request.Files.Count > 0)
        //    {
        //        try
        //        {
        //            HttpFileCollectionBase files = Request.Files;
        //            for (int i = 0; i < files.Count; i++)
        //            {
        //                HttpPostedFileBase file = files[i];
        //                string fname;
        //                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
        //                {
        //                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
        //                    fname = testfiles[testfiles.Length - 1];
        //                }
        //                else
        //                {
        //                    fname = file.FileName;
        //                }
        //                var newName = fname.Split('.');
        //                fname = newName[0] + "_" + DateTime.Now.Ticks.ToString() + "." + newName[1];
        //                var uploadRootFolderInput = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelUploads";
        //                Directory.CreateDirectory(uploadRootFolderInput);
        //                //var directoryFullPathInput = @"D:/Farhan/UploadFiles/UploadFiles/ExcelUploads/";
        //                fname = Path.Combine(uploadRootFolderInput, fname);
        //                file.SaveAs(fname);
        //                string xlsFile = fname;
        //                excelData = readExcel(fname);
        //            }
        //            if (excelData.Count > 0)
        //            {
        //                return Json(" Records Schedule Successfully !", JsonRequestBehavior.AllowGet);
        //            }
        //            else
        //                return Json("Somehting went wrong in reading File", JsonRequestBehavior.AllowGet);
        //            // return Json(false, JsonRequestBehavior.AllowGet);
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json(false, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    else
        //    {
        //        return Json(false, JsonRequestBehavior.AllowGet);
        //    }
        //}
       
        //public List<ExcelViewModel> readExcel(string FilePath)
        //{
        //    try
        //    {
        //        List<ExcelViewModel> excelData = new List<ExcelViewModel>();

        //        FileInfo existingFile = new FileInfo(FilePath);
        //        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        //        using (ExcelPackage package = new ExcelPackage(existingFile))
        //        {
        //            var currentSheet = package.Workbook.Worksheets;
        //            var worksheet = currentSheet.FirstOrDefault();
        //            // ExcelWorkbook workBook = package.Workbook;
        //            //ExcelWorksheet worksheet = workBook.Worksheets[1];

        //            int rowCount = worksheet.Dimension.End.Row;
        //            for (int row = 2; row <= rowCount; row++)
        //            {
        //                ExcelViewModel excelOBJ = new ExcelViewModel();
        //                excelOBJ.guid = worksheet.Cells[row, 1].Value.ToString();
        //                excelOBJ.reference = worksheet.Cells[row, 2].Value.ToString();
        //                excelOBJ.amount = worksheet.Cells[row, 3].Value.ToString();
        //                excelOBJ.count = worksheet.Cells[row, 4].Value.ToString();
        //                excelOBJ.interval = worksheet.Cells[row, 5].Value.ToString();
        //                excelOBJ.start_date = worksheet.Cells[row, 6].Value.ToString();

        //                excelData.Add(excelOBJ);
        //            }

        //        }
        //        return excelData;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        #endregion
    }
}