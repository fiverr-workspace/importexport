﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using e_klass.Models;
using Model.EF;
using Services.BLL;

namespace CustomsInstruction.Controllers
{
    public class HomeController : Controller
    {
        HomeViewModel homeVM = new HomeViewModel();
        HomeBLL homeBLL = new HomeBLL();
        bool flag = false;
        public ActionResult Index()
        {
            try
            {
                Session["Language"] = "en";
                if (flag != false)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
            catch
            {
                throw;
            }
        }

        public ActionResult Login()
        {
            List<SelectListItem> list = new List<SelectListItem> {
                new SelectListItem {Text="-- Select Role --",Value="" },
                new SelectListItem {Text="Client",Value="Client" },
                new SelectListItem {Text="Customer",Value="Customer" }
            };
            homeVM.ddlRoleList = list;
            return PartialView("Login", homeVM);
        }

        [HttpPost]
        public ActionResult Login(stam_Client client)                                                              //Synchronous login method.
        {
            try
            {
                //  ModelState.Clear();
                dynamic Getuser = "";
                if (client.Role == "Client")
                {
                    Getuser = homeBLL.VerifyClientLogin(client);
                    if (Getuser != null)
                    {
                        flag = true;
                        Session["UserId"] = Getuser.ClientId;
                        Session["ClientCode"] = Getuser.clientCode;
                        Session["Role"] = Getuser.Role;
                        Session["UserName"] = Getuser.UserName;

                        return PartialView("Dashboard", homeVM);
                    }
                    else
                    {
                        List<SelectListItem> list = new List<SelectListItem> {
                        new SelectListItem {Text="-- Select Role --",Value="" },
                        new SelectListItem {Text="Client",Value="Client" },
                        new SelectListItem {Text="Customer",Value="Customer" }
                        };
                        homeVM.ddlRoleList = list;
                        homeVM.ErrorMessage = "Invalid Username or Password or Role";
                        return View(homeVM);
                    }
                }
                else
                {
                    stam_Users user = new stam_Users();
                    user.Username = client.UserName;
                    user.Password = client.Password;
                    Getuser = homeBLL.VerifyCustomerLogin(user);
                    if (Getuser != null)
                    {
                        flag = true;
                        Session["UserId"] = Getuser.ClientId;
                        //  Session["ClientCode"] = Getuser.clientCode;
                        Session["Role"] = client.Role;
                        Session["UserName"] = Getuser.UserName;

                        return PartialView("Dashboard", homeVM);
                    }
                    else
                    {
                        homeVM.ErrorMessage = "Invalid Username or Password or Role";
                        List<SelectListItem> list = new List<SelectListItem> {
                        new SelectListItem {Text="-- Select Role --",Value="" },
                        new SelectListItem {Text="Client",Value="Client" },
                        new SelectListItem {Text="Customer",Value="Customer" }
                        };
                        homeVM.ddlRoleList = list;
                        return View(homeVM);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
        }


        public ActionResult Dashboard()                                                              //Synchronous login method.
        {
            try
            {
                //if (ValidateSession())
                //{
                //SchoolBLL schoolBLL = new SchoolBLL();
                //if (Session["SchoolId"] != null)
                //{
                //    SchoolId = Convert.ToInt32(Session["SchoolId"]);
                //}
                //homeVM.GetSchoolList = homeBLL.GetSchoolDetail();
                //homeVM.StudentCount = schoolBLL.GetStudentDetail(null).Count;
                //homeVM.LecturerCount = schoolBLL.GetLecturerDetail().Count;
                return View();
                //}
                //else
                //{
                //    return RedirectToAction("Login", "Home");
                //}
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpPost]
        public ActionResult SetLanguageSession(string language)
        {
            Session["Language"] = language;
            return Json("Set Language");
        }


        #region Company
        public ActionResult AddCompany()
        {
            //if (ValidateSession())
            //{
            homeVM.GetCompanyList = homeBLL.GetCompanyDetail(1);
            return PartialView("AddCompany", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        [HttpPost]
        public ActionResult SaveCompany(stam_Companies CompanyObj)
        {
            //if (ValidateSession())
            //{
            int Status = 0;
            CompanyObj.clientCode = "1";
            Status = homeBLL.SaveCompany(CompanyObj);

            if (Status == -1)
            {

                homeVM.ResponseMessage = "Company information Already Exists.";
                homeVM.ResponseColour = "text-red";
            }
            if (Status > 0)
            {
                // SendEmail(user.email, "New Account Created", "Welcome To e-klass.net");

                homeVM.ResponseMessage = "Company information saved successfully";
                homeVM.ResponseColour = "text-green";
            }
            homeVM.GetCompanyList = homeBLL.GetCompanyDetail(1);
            return PartialView("_ManageCompany", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        //public ActionResult ManageCompany()
        //{
        //    if (ValidateSession())
        //    {
        //        homeVM.GetUserList = homeBLL.GetCompanyDetail(1);
        //        return PartialView("_ManageCompany", homeVM);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //}

        public ActionResult IsCompanyNameExist(string UserName)
        {
            //if (ValidateSession())
            //{
            JavaScriptSerializer js = new JavaScriptSerializer();
            var data = homeBLL.IsCompanyNameExist(UserName);
            string nam = js.Serialize(data);
            var result = js.Deserialize<dynamic>(nam);
            return Json(result, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        public ActionResult DeleteCompany(int Id)
        {
            //if (ValidateSession())
            //{
            bool status;

            status = homeBLL.DeleteCompany(Id);
            homeVM.GetCompanyList = homeBLL.GetCompanyDetail(1);
            return PartialView("_ManageCompany", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        public ActionResult EditCompany(int? Id)
        {
            ////if (ValidateSession())
            ////{
            homeVM.CompanyObj = homeBLL.GetCompanyById(Convert.ToInt32(Id));
            homeVM.GetCompanyList = homeBLL.GetCompanyDetail(1);
            return PartialView("AddCompany", homeVM);
            //}
            //else
            //{
            //   return RedirectToAction("Login", "Home");
            //}

        }
        #endregion

        #region User
        public ActionResult AddUser()
        {
            //if (ValidateSession())
            //{
            homeVM.GetUserList = homeBLL.GetUserDetail();
            return PartialView("AddUser", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        [HttpPost]
        public ActionResult SaveUser(stam_Users UserObj)
        {
            //if (ValidateSession())
            //{
            int Status = 0;

            Status = homeBLL.SaveUser(UserObj);

            if (Status == -1)
            {

                homeVM.ResponseMessage = "User information Already Exists.";
                homeVM.ResponseColour = "text-red";
            }
            if (Status > 0)
            {
                // SendEmail(user.email, "New Account Created", "Welcome To e-klass.net");

                homeVM.ResponseMessage = "User information saved successfully";
                homeVM.ResponseColour = "text-green";
            }
            homeVM.GetUserList = homeBLL.GetUserDetail();
            return PartialView("_ManageUser", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        //public ActionResult ManageCompany()
        //{
        //    if (ValidateSession())
        //    {
        //        homeVM.GetUserList = homeBLL.GetCompanyDetail(1);
        //        return PartialView("_ManageCompany", homeVM);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //}

        public ActionResult IsUserNameExist(string UserName)
        {
            //if (ValidateSession())
            //{
            JavaScriptSerializer js = new JavaScriptSerializer();
            var data = homeBLL.IsUserNameExist(UserName);
            string nam = js.Serialize(data);
            var result = js.Deserialize<dynamic>(nam);
            return Json(result, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        public ActionResult DeleteUser(int Id)
        {
            //if (ValidateSession())
            //{
            bool status;

            status = homeBLL.DeleteUser(Id);
            homeVM.GetUserList = homeBLL.GetUserDetail();
            return PartialView("_ManageUser", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        public ActionResult EditUser(int? Id)
        {
            ////if (ValidateSession())
            ////{
            homeVM.UserObj = homeBLL.GetUserById(Convert.ToInt32(Id));
            homeVM.GetUserList = homeBLL.GetUserDetail();
            return PartialView("AddUser", homeVM);
            //}
            //else
            //{
            //   return RedirectToAction("Login", "Home");
            //}

        }
        #endregion

        #region Customer
        public ActionResult AddCustomer()
        {
            //if (ValidateSession())
            //{
            List<SelectListItem> list = new List<SelectListItem> {
                new SelectListItem {Text="IMPORTER",Value="IMPORTER" },
                new SelectListItem {Text="EXPORTER",Value="EXPORTER" },
                new SelectListItem {Text="CONSIGNEE",Value="CONSIGNEE" }
            };
            homeVM.ddlPartiesList = list;
            homeVM.GetCustomerList = homeBLL.GetCustomerDetail(1);
            return PartialView("AddCustomer", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        [HttpPost]
        public ActionResult SaveCustomer(party CustomerObj)
        {
            //if (ValidateSession())
            //{
            int Status = 0;

            Status = homeBLL.SaveCustomer(CustomerObj);

            if (Status == -1)
            {

                homeVM.ResponseMessage = "Customer information Already Exists.";
                homeVM.ResponseColour = "text-red";
            }
            if (Status > 0)
            {
                // SendEmail(user.email, "New Account Created", "Welcome To e-klass.net");

                homeVM.ResponseMessage = "Customer information saved successfully";
                homeVM.ResponseColour = "text-green";
            }
            homeVM.GetCustomerList = homeBLL.GetCustomerDetail(1);
            return PartialView("_ManageCustomer", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        //public ActionResult ManageCompany()
        //{
        //    if (ValidateSession())
        //    {
        //        homeVM.GetUserList = homeBLL.GetCompanyDetail(1);
        //        return PartialView("_ManageCompany", homeVM);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //}

        public ActionResult IsCustomerNameExist(string UserName)
        {
            //if (ValidateSession())
            //{
            JavaScriptSerializer js = new JavaScriptSerializer();
            var data = homeBLL.IsCustomerNameExist(UserName);
            string nam = js.Serialize(data);
            var result = js.Deserialize<dynamic>(nam);
            return Json(result, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        public ActionResult DeleteCustomer(int Id)
        {
            //if (ValidateSession())
            //{
            bool status;

            status = homeBLL.DeleteCustomer(Id);
            homeVM.GetCustomerList = homeBLL.GetCustomerDetail(1);
            return PartialView("_ManageCustomer", homeVM);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Home");
            //}
        }

        public ActionResult EditCustomer(int? Id)
        {
            ////if (ValidateSession())
            ////{
            homeVM.CustomerObj = homeBLL.GetCustomerById(Convert.ToInt32(Id));
            homeVM.GetCustomerList = homeBLL.GetCustomerDetail(1);
            return PartialView("AddUser", homeVM);
            //}
            //else
            //{
            //   return RedirectToAction("Login", "Home");
            //}

        }
        #endregion
    }
}