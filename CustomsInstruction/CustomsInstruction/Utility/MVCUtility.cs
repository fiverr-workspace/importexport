﻿using Model.EF;
using Services.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomsInstruction.Utility
{
    public class MVCUtility
    {
        HomeBLL homeBLL = new HomeBLL();
        public SelectList GetUserList()
        {
            List<stam_Users> dataList = new List<stam_Users>();
            dataList = homeBLL.GetUserDetail().ToList();
            List<SelectListItem> ddlDataList = new List<SelectListItem>();
            foreach (stam_Users s in dataList)
            {
                ddlDataList.Add(new SelectListItem { Selected = false, Text = s.Username , Value = s.UserID.ToString() });
            }
            return new SelectList(ddlDataList, "Value", "Text", null);
        }
        
    }
}