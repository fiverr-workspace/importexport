USE [Translima]
GO
/****** Object:  Table [dbo].[stam_Client]    Script Date: 1/23/2021 3:45:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stam_Client](
	[ClientId] [int] IDENTITY(1,1) NOT NULL,
	[clientCode] [varchar](25) NOT NULL,
	[CustomerName] [varchar](75) NULL,
	[Active] [bit] NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_stam_Client] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[stam_Companies]    Script Date: 1/23/2021 3:45:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stam_Companies](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[clientCode] [nchar](10) NOT NULL,
	[CompanyCode] [nchar](20) NOT NULL,
	[TraderID] [nchar](20) NOT NULL,
	[Name] [nchar](120) NULL,
	[Address] [nchar](70) NULL,
	[PostalCode] [nchar](9) NULL,
	[City] [nchar](35) NULL,
	[CountryCode] [nchar](2) NULL,
	[Phone] [nchar](20) NULL,
	[Email] [nchar](100) NULL,
	[PeachActive] [bit] NOT NULL,
 CONSTRAINT [PK_stam_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[stam_Users]    Script Date: 1/23/2021 3:45:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stam_Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[clientCode] [nchar](10) NOT NULL,
	[UserID] [nchar](10) NOT NULL,
	[Username] [nchar](20) NOT NULL,
	[Password] [nchar](20) NOT NULL,
	[Email] [nchar](100) NULL,
	[ChangePasswordAtNextLogin] [bit] NULL,
	[Remark] [nchar](250) NULL,
	[Blocked] [bit] NULL,
	[PasswordValidityDays] [nchar](10) NULL,
 CONSTRAINT [PK_stam_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[stam_Client] ON 

INSERT [dbo].[stam_Client] ([ClientId], [clientCode], [CustomerName], [Active], [UserName], [Password]) VALUES (1, N'BONGERS', N'Bongers Expeditie B.V.', 1, N'BONGERS', N'12345678')
INSERT [dbo].[stam_Client] ([ClientId], [clientCode], [CustomerName], [Active], [UserName], [Password]) VALUES (2, N'RETAILSOL', N'Retail Solutions B.V.', 1, NULL, NULL)
INSERT [dbo].[stam_Client] ([ClientId], [clientCode], [CustomerName], [Active], [UserName], [Password]) VALUES (3, N'ECC', N'European Customs Clearance B.V.', 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[stam_Client] OFF
SET IDENTITY_INSERT [dbo].[stam_Companies] ON 

INSERT [dbo].[stam_Companies] ([CompanyId], [clientCode], [CompanyCode], [TraderID], [Name], [Address], [PostalCode], [City], [CountryCode], [Phone], [Email], [PeachActive]) VALUES (1, N'1         ', N'BE33                ', N'1123                ', N'Test comp                                                                                                               ', N'adsda                                                                 ', N'24       ', N'avc                                ', N'24', N'033347218447        ', N'test@gmail.com                                                                                      ', 1)
SET IDENTITY_INSERT [dbo].[stam_Companies] OFF
SET IDENTITY_INSERT [dbo].[stam_Users] ON 

INSERT [dbo].[stam_Users] ([Id], [clientCode], [UserID], [Username], [Password], [Email], [ChangePasswordAtNextLogin], [Remark], [Blocked], [PasswordValidityDays]) VALUES (1, N'BONGERS   ', N'RICHARD   ', N'RICHARD             ', N'RICHARD             ', N'richard.groenendijk@aeb.com                                                                         ', NULL, NULL, NULL, NULL)
INSERT [dbo].[stam_Users] ([Id], [clientCode], [UserID], [Username], [Password], [Email], [ChangePasswordAtNextLogin], [Remark], [Blocked], [PasswordValidityDays]) VALUES (2, N'BONGERS   ', N'FRANK     ', N'FRANK               ', N'FRANK               ', N'frank@fbcatlas.nl                                                                                   ', NULL, NULL, NULL, NULL)
INSERT [dbo].[stam_Users] ([Id], [clientCode], [UserID], [Username], [Password], [Email], [ChangePasswordAtNextLogin], [Remark], [Blocked], [PasswordValidityDays]) VALUES (3, N'1242      ', N'1         ', N'test_user           ', N'12345678            ', N'test@gmail.com                                                                                      ', NULL, N'sdsdfsf                                                                                                                                                                                                                                                   ', NULL, NULL)
SET IDENTITY_INSERT [dbo].[stam_Users] OFF
