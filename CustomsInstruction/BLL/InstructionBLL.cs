﻿using DAL;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class InstructionBLL
    {
        InstructionDAL instructionDAL = new InstructionDAL();
        public int SaveInstruction(commercial commercial, party partyObj, commercial_extra commercial_extraObj, item itemobj, items_packages itemPackeagesObj, items_extra items_extra)
        {
            try
            {
                return instructionDAL.SaveInstruction(commercial, partyObj, commercial_extraObj, itemobj, itemPackeagesObj, items_extra); ;
            }
            catch
            {
                throw;
            }
        }
    }
}
