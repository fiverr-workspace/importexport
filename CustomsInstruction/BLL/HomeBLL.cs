﻿using Model.EF;
using Repository.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.BLL
{
   
    public class HomeBLL
    {
        HomeDAL homeDAL = new HomeDAL();
        public stam_Client VerifyClientLogin(stam_Client login)
        {
            try
            {
                return homeDAL.VerifyClientLogin(login);
            }
            catch
            {
                throw;
            }
        }


        #region Company
        public int SaveCompany(stam_Companies comp)
        {
            try
            {
                return homeDAL.SaveCompany(comp);
            }
            catch
            {
                throw;
            }
        }

        public List<stam_Companies> GetCompanyDetail(int RoleId)
        {
            try
            {
                return homeDAL.GetCompanyDetail(RoleId);
            }
            catch
            {
                throw;
            }
        }

        public bool IsCompanyNameExist(string UserName)
        {
            try
            {
                return homeDAL.IsCompanyNameExist(UserName);
            }
            catch
            {
                throw;
            }
        }


        public bool DeleteCompany(int Id)
        {
            try
            {
                return homeDAL.DeleteCompany(Id);
            }
            catch
            {
                throw;
            }
        }

        public stam_Companies GetCompanyById(int cid)
        {
            try
            {
                return homeDAL.GetCompanyById(cid);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region User
        public int SaveUser(stam_Users comp)
        {
            try
            {
                return homeDAL.SaveUser(comp);
            }
            catch
            {
                throw;
            }
        }

        public List<stam_Users> GetUserDetail()
        {
            try
            {
                return homeDAL.GetUserDetail();
            }
            catch
            {
                throw;
            }
        }

        public bool IsUserNameExist(string UserName)
        {
            try
            {
                return homeDAL.IsUserNameExist(UserName);
            }
            catch
            {
                throw;
            }
        }


        public bool DeleteUser(int Id)
        {
            try
            {
                return homeDAL.DeleteUser(Id);
            }
            catch
            {
                throw;
            }
        }

        public stam_Users GetUserById(int cid)
        {
            try
            {
                return homeDAL.GetUserById(cid);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Customer

        public stam_Users VerifyCustomerLogin(stam_Users login)
        {
            try
            {
                return homeDAL.VerifyCustomerLogin(login);
            }
            catch
            {
                throw;
            }
        }
        public int SaveCustomer(party comp)
        {
            try
            {
                return homeDAL.SaveCustomer(comp);
            }
            catch
            {
                throw;
            }
        }

        public List<party> GetCustomerDetail(int RoleId)
        {
            try
            {
                return homeDAL.GetCustomerDetail(RoleId);
            }
            catch
            {
                throw;
            }
        }

        public bool IsCustomerNameExist(string CustomerName)
        {
            try
            {
                return homeDAL.IsCustomerNameExist(CustomerName);
            }
            catch
            {
                throw;
            }
        }


        public bool DeleteCustomer(int Id)
        {
            try
            {
                return homeDAL.DeleteCustomer(Id);
            }
            catch
            {
                throw;
            }
        }

        public party GetCustomerById(int cid)
        {
            try
            {
                return homeDAL.GetCustomerById(cid);
            }
            catch
            {
                throw;
            }
        }
        #endregion

    }
}
