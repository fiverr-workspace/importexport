//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class history
    {
        public System.DateTime dateTime { get; set; }
        public string key { get; set; }
        public string table { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }
}
